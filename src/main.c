
#define __USE_MINGW_ANSI_STDIO 1

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "time.h"

#include "common.h"
#include "vector.h"
#include "lex.h"

struct timer
{
    clock_t start;

    double seconds_passed;
};

struct timer time_start( )
{
    struct timer result;
    result.start = clock();
    result.seconds_passed = 0;
    return result;
}

void time_end( struct timer* timer )
{
    timer->seconds_passed = (clock() - timer->start) / (double)CLOCKS_PER_SEC;
}

int main( int argc, char** argv )
{
    set_log_level( LOG_DIAGNOSTIC );

    struct timer overall_time = time_start();
    
    if ( argc < 2 )
    {
        compile_log( LOG_CRITICAL, LOG_ERROR, "No input file given." );
        return 1;
    }

    struct token_set tokens;

    const char* filename = argv[1];
    
    compile_log( LOG_NORMAL, LOG_MESSAGE, "Lexing file '%s'.", filename );
    struct timer lex_timer = time_start();
    if ( lex_file( &tokens, argv[1] ) != RETURN_SUCCESS )
    {
        compile_log( LOG_CRITICAL, LOG_ERROR, "Could not lex file '%s'.", filename );
        return 1;
    }

    time_end( &lex_timer );
    compile_log( LOG_NORMAL, LOG_MESSAGE, "Lexing time elapsed: %lfs.", lex_timer.seconds_passed );
    compile_log( LOG_VERBOSE, LOG_MESSAGE, "Lexed file '%s' into %u tokens.", filename, tokens.tokens.elcount );
    compile_log( LOG_VERBOSE, LOG_MESSAGE, "Consumed identifier space: %u bytes.", tokens.identifiers.capacity );

    int i;
    for ( i = 0; i < tokens.tokens.elcount; i++ )
    {
        enum token_type type = token_get_type( &tokens, i );
        char* identifier = token_get_identifier( &tokens, i );
        const char* separator = identifier ? ", " : "";
        identifier = identifier ? identifier : "";
        
        compile_log( LOG_VERBOSE, LOG_MESSAGE, "Token %d: (%s%s%s)",
                     i, token_type_names[type], separator, identifier );
    }

    lex_destroy( &tokens );

    time_end( &overall_time );
    compile_log( LOG_NORMAL, LOG_MESSAGE, "Overall time elapsed: %lfs.", overall_time.seconds_passed );
    
    return 0;
}
