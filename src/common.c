
#include "stdio.h"
#include "stdarg.h"

#include "common.h"

enum log_level current_level = LOG_NORMAL;

void set_log_level( enum log_level level )
{
    current_level = level;
}

void compile_log( enum log_level level, enum log_type type, const char* fmt, ... )
{
    if ( level <= current_level )
    {
        switch ( type )
        {
        case LOG_MESSAGE:
            break;

        case LOG_WARNING:
            printf( "[Warning] " );
            break;

        case LOG_ERROR:
            printf( "[Error] " );
            break;

        case LOG_ERROR_INTERNAL:
            printf( "[Internal Error] " );
            break;

        default:
            break;
        }

        va_list arg_list;
        va_start( arg_list, fmt );
        vprintf( fmt, arg_list );
        va_end( arg_list );

        printf( "\n" );
    }
}
