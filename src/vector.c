
#include "stdlib.h"
#include "string.h"

#include "vector.h"

struct vector v_create_empty( size_t elsize )
{
    struct vector result;

    result.data = NULL;
    result.elsize = elsize;
    result.elcount = 0;
    result.capacity = 0;

    return result;
}

struct vector v_create_default( size_t elsize )
{
    return v_create( elsize, V_DEFAULT_CAPACITY );
}

struct vector v_create( size_t elsize, uint32_t capacity )
{
    struct vector result = v_create_empty( elsize );

    v_resize( &result, capacity );

    return result;
}

void v_destroy( struct vector* vect )
{
    if ( vect != NULL )
    {
        if ( vect->data != NULL )
        {
            free( vect->data );
        }
        
        vect->data = NULL;
        vect->capacity = 0;
        vect->elcount = 0;
    }
}

int v_resize( struct vector* vect, uint32_t capacity )
{
    if ( capacity > 0 )
    {
        void* data = realloc( vect->data, vect->elsize * capacity );
        if ( !data )
        {
            compile_log( LOG_CRITICAL, LOG_ERROR_INTERNAL, "Failed to allocate vector data." );
            free( data );
            return RETURN_FAIL;
        }

        if ( capacity < vect->elcount )
        {
            vect->elcount = capacity;
        }
        
        vect->data = data;
        vect->capacity = capacity;        
    }
    else
    {
        free( vect->data );
        vect->data = NULL;
        vect->capacity = 0;
        vect->elcount = 0;
    }

    return RETURN_SUCCESS;
}

void* v_reserve( struct vector* vect, uint32_t count )
{
    uint32_t result_index = vect->elcount; 

    if ( vect->elcount + count > vect->capacity )
    {
        // At least double the size of the vector.
        uint32_t min_capacity = 2 * vect->capacity;
        if ( vect->elcount + count > min_capacity )
        {
            min_capacity = vect->elcount + count;
        }

        if ( v_resize( vect, min_capacity ) != RETURN_SUCCESS )
        {
            compile_log( LOG_CRITICAL, LOG_ERROR_INTERNAL, "Could not reserve space in vector." );
            return NULL;
        }
    }

    vect->elcount += count;

    return v_get( vect, result_index );
}

int v_push( struct vector* vect, const void* item )
{
    void* result_location = v_reserve( vect, 1 );
    if ( !result_location )
    {
        compile_log( LOG_CRITICAL, LOG_ERROR_INTERNAL, "Could not push item to vector." );
        return RETURN_FAIL;
    }
    
    memcpy( result_location, item, vect->elsize );

    return RETURN_SUCCESS;
}

void* v_get( const struct vector* vect, uint32_t index )
{
    if ( index >= vect->elcount )
    {
        compile_log( LOG_CRITICAL, LOG_ERROR_INTERNAL, "Vector index %u was out of bounds.", index );
        return NULL;
    }
    
    return vect->data + vect->elsize * index;
}
