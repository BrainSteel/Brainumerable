
#include "ctype.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "common.h"
#include "lex.h"

const char* token_type_names[] =
{
#define X( val, str ) #val,
    X_TOKENS
#undef X
};

static struct
{
    enum token_type type;
    const char* symbol;
} symbol_map[] =
{
#define X( val, str ) { val, str },
    X_TOKEN_SYMBOLS
#undef X
};

struct word_reader
{
    FILE* file;
    char* cache_ptr;
    int cache_len;
    int cache_col_count;
    char cache_buf[1024];
};

#define X( val, str ) str
static char token_symbols[] = X_TOKEN_SYMBOLS;
#undef X

// Calculates the number of whitespace text columns taken up by the first
// set of whitespace within the string.
// This function assumes a tab stop every 8 columns.
static int calculate_columns( const char* str, int offset )
{
    int cur_col = 0;
    
    while ( *str && isspace( *str ) )
    {
        if ( *str == '\n' )
        {
            cur_col = 0;
        }
        else if ( *str == '\t' )
        {
            int last_tab_stop = (offset + cur_col) & 0xFFFFFFF8;
            cur_col = last_tab_stop + 8 - offset;
        }
        else
        {
            cur_col++;
        }

        str++;
    }

    return cur_col;
}

static int refill_word_cache( struct word_reader* reader )
{
    if ( fgets( reader->cache_buf, SIZE_ARRAY( reader->cache_buf ), reader->file ) )
    {
        reader->cache_len = strlen( reader->cache_buf );
        reader->cache_ptr = reader->cache_buf;
        return 1;
    }
    else
    {
        return 0;
    }
}

// Attempts to read a word, and reports whether the word was successfully
// filled in.
static int read_word( struct word_reader* reader,
                      // Maximum length of the output word, including null termination.
                      int max_word_len,
                      // Pointer filled in with the word 
                      char* filled_word,
                      // Indicates the number of lines that were ended before finding the word.
                      int* lines_consumed,
                      // Columns consumed until the start of the word is found
                      int* columns_consumed )
{
    static char word_delims[32] = { 0 };
    if ( word_delims[0] == 0 )
    {
        sprintf( word_delims, " \r\n\t%s%c", token_symbols, COMMENT_CHAR );
    }
    
    int word_found = 0;
    int line_count = 0;
    int column_count = 0;

    if ( max_word_len <= 1 )
    {
        // At best, we can fill in a null termination.
        if ( max_word_len == 1 )
        {
            *filled_word = 0;
        }
        
        goto return_results;
    }

    if ( reader->cache_len == 0 )
    {
        if ( !refill_word_cache( reader ) )
        {
            goto return_results;
        }
    }

    char first;
    int whitespace_len;
    int word_len;
    
    int read = sscanf( reader->cache_ptr, " %n%c", &whitespace_len, &first );
    
    if ( read == 1 )
    {
        column_count = calculate_columns( reader->cache_ptr, reader->cache_col_count );
        
        word_found = 1;
        if ( first == COMMENT_CHAR || strchr( token_symbols, first ) )
        {
            // The found word is a symbol.
            word_len = 1;
            filled_word[0] = first;
            filled_word[1] = 0;
        }
        else
        {
            char* word_ptr = reader->cache_ptr + whitespace_len;

            char scanf_format[256] = { 0 };
            sprintf( scanf_format, "%%%d[^%s]%%n", max_word_len - 1, word_delims );
            
            sscanf( word_ptr, scanf_format, filled_word, &word_len );
        }

        reader->cache_col_count += column_count + word_len;
        
        int total_len = whitespace_len + word_len;
        if ( total_len + (reader->cache_ptr - reader->cache_buf) == SIZE_ARRAY( reader->cache_buf ) - 1 )
        {
            // The word cut off because the line is wider than the cache buffer.
            if ( refill_word_cache( reader ) && !strchr( word_delims, reader->cache_ptr[0] ) )
            {
                read_word( reader, max_word_len - word_len, filled_word + word_len, NULL, NULL );                
            }
        }
        else
        {
            // purge the cache up to the end of the read word.
            reader->cache_ptr += total_len;
            reader->cache_len -= total_len;
        }
    }
    else
    {
        // We only read whitespace before, so continue reading.
        if ( reader->cache_ptr[whitespace_len - 1] == '\n' )
        {
            // If we read the end of a line, we consider the line to be consumed
            reader->cache_col_count = 0;
            line_count++;
        }
        else
        {
            column_count = calculate_columns( reader->cache_ptr, reader->cache_col_count );
            reader->cache_col_count += column_count;
        }

        // Setting cache_len to 0 forces the cache to be refilled with the next line.
        reader->cache_len = 0;

        int recur_lines_consumed;
        int recur_columns_consumed;
        // Try to read a word on the next line
        word_found = read_word( reader, max_word_len, filled_word, &recur_lines_consumed, &recur_columns_consumed );

        // Only consider the columns consumed from the last line that we read from.
        line_count += recur_lines_consumed;
        if ( line_count > 0 )
        {
            column_count = recur_columns_consumed;
        }
        else
        {
            column_count += recur_columns_consumed;
        }
    }

return_results:    
    if ( lines_consumed )
    {
        *lines_consumed = line_count;
    }

    if ( columns_consumed )
    {
        *columns_consumed = column_count;
    }

    return word_found;
}

static int push_reserved( struct token_set* tokens, enum token_type type, struct file_location location )
{
    struct token to_push;
    to_push.type = type;
    to_push.identifier = IDENTIFIER_INVALID;
    to_push.location.line = location.line;
    to_push.location.col = location.col;

    return v_push( &tokens->tokens, &to_push );
}

static int push_identifier( struct token_set* tokens, const char* identifier, struct file_location location )
{
    struct token to_push;
    to_push.type = TOKEN_IDENTIFIER;
    to_push.location.line = location.line;
    to_push.location.col = location.col;
    
    int len = strlen( identifier );
    void* reserved = v_reserve( &tokens->identifiers, len + 1 );
    
    if ( !reserved )
    {
        compile_log( LOG_CRITICAL, LOG_ERROR_INTERNAL, "Failed to allocate token identifier." );
        return RETURN_FAIL;
    }
    
    strcpy( reserved, identifier );
    to_push.identifier = reserved - tokens->identifiers.data;

    return v_push( &tokens->tokens, &to_push );
}

int lex_file( struct token_set* tokens, const char* filename )
{
    struct word_reader reader = { 0 };
    reader.file = fopen( filename, "r" );
    if ( !reader.file )
    {
        compile_log( LOG_CRITICAL, LOG_ERROR, "Failed to read file '%s' for tokenization.", filename );
        return RETURN_FAIL;  
    }
    
    tokens->identifiers = v_create_default( 1 );
    tokens->tokens = v_create_default( sizeof( struct token ) );
    
    int return_value = RETURN_SUCCESS;

    struct file_location current_location;
    current_location.line = 1;
    current_location.col = 1;
    
    int lines_read, col_read;
    int dont_read_next = 0;

    char word[256];

    while ( dont_read_next || read_word( &reader, SIZE_ARRAY( word ), word, &lines_read, &col_read ) )
    {
        dont_read_next = 0;
        
        int word_len = strlen( word );
        current_location.line += lines_read;
        if ( lines_read > 0 )
        {
            current_location.col = 1;
        }
        current_location.col += col_read;

        struct file_location token_location = current_location;

        current_location.col += word_len;
        
        compile_log( LOG_DIAGNOSTIC, LOG_MESSAGE, "Read word: '%s' (line %d, col %d).",
                     word, token_location.line, token_location.col );
        
        char* symbol_char;
        if ( word[0] == COMMENT_CHAR )
        {
            // Handle a comment...
            lines_read = 0;
            col_read = 0;

            // We will have already found the next word to parse, so skip the word-read in the
            // next line.
            dont_read_next = 1;
            int found_word = 0;
            while ( lines_read == 0 )
            {
                found_word = read_word( &reader, SIZE_ARRAY( word ), word, &lines_read, &col_read );
                if ( !found_word )
                {
                    break;
                }
            }

            if ( !found_word )
            {
                break;
            }

            continue;
        }
        else if ( (symbol_char = strchr( token_symbols, word[0] )) )
        {
            // Handle a symbol token...
            int symbol_index = symbol_char - token_symbols;
            if ( push_reserved( tokens, symbol_map[symbol_index].type, token_location ) != RETURN_SUCCESS )
            {
                return_value = RETURN_FAIL;
                goto done;
            }
        }
#define X( val, str )                                                               \
        /* Handle any reserved keyword...  */                                       \
        else if ( strcmp( word, str ) == 0 )                                        \
        {                                                                           \
            if ( push_reserved( tokens, val, token_location ) != RETURN_SUCCESS )   \
            {                                                                       \
                return_value = RETURN_FAIL;                                         \
                goto done;                                                          \
            }                                                                       \
        }

        X_TOKEN_KEYWORDS
#undef X
        else 
        {
            // Handle an arbitrary identifier...
            if ( push_identifier( tokens, word, token_location ) != RETURN_SUCCESS )
            {
                return_value = RETURN_FAIL;
                goto done;
            }
        }
    }

done:
    fclose( reader.file );
    return return_value;
}

void lex_destroy( struct token_set* set )
{
    if ( set )
    {
        v_destroy( &set->tokens );
        v_destroy( &set->identifiers );
    }
}

static struct token* get_token( struct token_set* set, uint32_t index )
{
    struct token* result = v_get( &set->tokens, index );
    if ( !result )
    {
        compile_log( LOG_CRITICAL, LOG_ERROR_INTERNAL, "Failed to retrieve token at index %d.", index );
    }

    return result;
}

enum token_type token_get_type( struct token_set* set, uint32_t index )
{
    struct token* token = get_token( set, index );
    if ( !token )
    {
        return -1;
    }

    return token->type;
}

char* token_get_identifier( struct token_set* set, uint32_t index )
{
    struct token* token = get_token( set, index );
    if ( !token )
    {
        return NULL;
    }

    if ( token->identifier == IDENTIFIER_INVALID )
    {
        return NULL;
    }
    
    return set->identifiers.data + token->identifier;
}
        
