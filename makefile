
LDIR := "$(MINGW_LIB)"

SDIR := src
IDIR := inc

CFLAGS := -g -Wall -Werror -I$(IDIR)

SOURCES := main.c vector.c common.c lex.c
HEADERS := common.h vector.h lex.h

OBJECTS := $(SOURCES:%.c=%.o)

bn.exe: $(OBJECTS)
	gcc $^ -o $@

%.o: $(SDIR)/%.c $(HEADERS:%.h=$(IDIR)/%.h)
	gcc $(CFLAGS) -c $< -o $@

example_asm.exe: bnlib.s
	as -o bnlib.o $<
	ld -o $@ -L$(LDIR) bnlib.o -lkernel32

.PHONY: clean

clean:
	-del $(OBJECTS)
	-del example_asm.exe
	-del bnc.exe
