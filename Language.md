# The Brainumerable Language Specification

## 1 Introduction
The Brainumerable language is the lovechild of a true turing tarpit and high
level programming--a rare, and possibly contradictory combination. 
The name derives from the concepts that inspired the language:
 - [BrainF**k](https://en.wikipedia.org/wiki/Brainfuck): An esoteric
language originally designed to be an extremely minimalist, yet turing-complete,
programming language. Brainumerable strives to capture the spirit of BF in its
simplicity and bare-bones features.
 - [Enumerables](https://en.wikipedia.org/wiki/Iterator): That is, collections 
that can create objects (called _enumerators_) that can iterate through items in
the collection. Some languages, like Python, describe the same concept with 
_iterables_ and _iterators_. The description of enumerables in the Brainumerable 
language is heavily inspired by their usage in .NET. Equally important as the
enumerables themselves are the functions, or 
[generators](https://en.wikipedia.org/wiki/Iterator#Generators), that produce
them. Brainumerable posits that the acts of generation and enumeration are
sufficient for turing-completeness.

## 2 Type System

There is one type in the Brainumerable language: The `enumerable` type.
All objects are implicitly `enumerable`, and each element in an `enumerable`
is itself `enumerable`. There is one literal: `null`, which represents an
`enumerable` that contains no elements. Enumerating `null` is legal, and will
do nothing.

All `enumerable` objects are immutable and passed by reference. The _references_
to `enumerable`s are mutable, and may change to point to any other `enumerable`.
Note that this means that there is no guarantee that enumerating an `enumerable`
twice will yield identical results, as the `enumerable` may depend on arbitrary 
external references in its calculation of the elements.

Because all objects share the same type, there is no need to clutter the syntax
with type names.

## 3 Variables

A variable in the Brainumerable language contains a reference to an 
`enumerable`. Variables are not required to be initialized. If a variable is
referenced before it is initialized, the default value of `null` is retrieved.
Variables belong to their closest enclosing scope, and may be
declared and initialized at the global level.

Global variables are considered
constant, and may not be changed from their initial value. In the execution of
the program, the first step is the creatioin of all global variables, in
the order they are declared in the program. Once a global variable is assigned,
it is automatically cached so that enumerating it will always produce identical
results.

## 4 Methods

A method is a named collection of statements to be executed in order, using a 
predefined set of inputs. Methods may be recursive.
There are three kinds of methods in the Brainumerable language:

1. Subroutines: methods that produce no result. These methods are declared with
the `void` keyword.
2. Functions: methods that produce a result using the `return` keyword. Through
some means or another, a function produces an `enumerable` object and `return`s
to the caller a reference to it. These methods are declared with the
`function` keyword.
3. Generators: methods that _are themselves_ `enumerable`, and produce elements 
one at a time using the `yield` keyword. Generators are not executed until they 
are enumerated, and are only executed until the next `yield` statement, at which
point the `yield`ed element is returned as the next item in the collection.
When the last statement of the method is executed, the end of the collection
has been reached. These methods are declared with the `generator` keyword.

### 4.1 Special Methods

#### **4.1.1 `void main()`**
The `main` subroutine is the entry point of the program, and is called 
immediately after initialization of any global variables. `main` should not take
any arguments, and must be declared `void`. A program is not valid unless the 
`main` method is declared.

#### **4.1.2 `function in()`**
The `in` function returns an `enumerable` containing `n` elements, where `n` is
the ASCII value of the next character read from `stdin`.

#### **4.1.3 `void out( char )`**
The `out` subroutine writes the character whose ASCII value is `n` to `stdout`,
where `n` is the number of elements in `char`. 

## 5 Syntax

### 5.1 Comments
```
# Brainumerable supports single-line comments starting with a '#' symbol.
# Any text after a '#' is ignored until the end of a line.
```

### 5.2 Methods

#### **5.2.1 Subroutines**
```
void method_name( arg_1, arg_2, ..., arg_n )
{
    # Statements...
}
```

Declares a subroutine `method_name` taking arguments `arg_1`, `arg_2`, etc.

#### **5.2.2 Functions**
```
function function_name( arg_1, arg_2, ..., arg_n )
{
    # Statements...
}
```
Declares a function `function_name` taking arguments `arg_1`, `arg_2`, etc.

#### **5.2.3 Generators**
```
generator generator_name( arg_1, arg_2, ..., arg_n )
{
    # Statements...
}
```
Declares a generator `generator_name` taking arguments `arg_1`, `arg_2`, etc.

### 5.3 Statements

#### **5.3.1 Variable Declaration**
```
identifier;
```

Declares the variable `identifier` in the current scope. 
If the identifier already exists as a 
variable in an accessible scope to the current statement, an error is generated
at parse time. If the variable `identifier` is referenced before it is set, the
retrieved value is `null`. A variable declaration is valid in the global scope.

#### **5.3.2 Variable Assignment**
```
identifier = rvalue;
```

Sets the variable `identifier` to a reference to the `enumerable` produced by
the `rvalue` expression. If the `identifier` variable has not yet been declared,
the assignment is considered to be the declaration of `identifier`. A variable
assignment is valid in the global scope.

#### **5.3.3 Scope Declaration**
```
{
    # Arbitrary statements in a new scope...
}
```
An arbitrary scope may be created within a method to contain temporary
variables. The variables declared within a scope are not accessible from outside
of that scope.

#### **5.3.4 Return Statement**
```
return;
```

Exits execution of a subroutine (a `void` method) immediately and passes control
back to the caller. 

This statement is not valid within a function or generator.

#### **5.3.5 Return Statement (With Value)**
```
return rvalue;
```

Exits execution of a function and returns the value of the `enumerable` pointed
to by the `rvalue` expression to the caller of the current function.

This statement is not valid in a subroutine or generator.

#### **5.3.6 Yield Statement**
```
yield rvalue;
```

Yields the value of the `enumerable` pointed to by the `rvalue` expression to
the `for` statement enumerating the generator. When this statement is executed,
control is passed back to the statement that is enumerating the generator,
and control returns back to the generator when the enumerating statement 
requests the next element, at the line immediately following the `yield` 
statement.

This statement is not valid in a subroutine or function.

#### **5.3.7 For Statement**
```
for ( item : collection )
{
    # Statements...
}
```

Iterates through `collection`. The variable `item` is assigned to each element
in `collection`, in order, and the statements within the `for` statement are 
executed. The `for` statement introduces its own scope, and the `item` variable
is considered to be part of that new scope. Execution of a `for` statement may
be interrupted by a `return` statement. The braces around the statements within
the `for` scope are optional for single-line `for` statements.

#### **5.3.8 Call Statement**
```
method_name( rvalue_1, rvalue_2, ..., rvalue_n )
```

Calls the method `method_name`, passing expressions `rvalue_1`, `rvalue_2`, etc
as arguments. If the method is a function or generator, the resulting 
`enumerable` is discarded. Because a generator uses deferred execution, calling 
a generator in this way always does nothing.

#### **5.3.9 Cache Statement**
```
^rvalue;
```

Fully enumerates the expression `rvalue`.

### 5.4 Expressions

Expressions produce values for use in statements. The identifier of any variable
may be considered to be an expression producing the value of that variable.

#### **5.4.1 Null Expression**
```
null
```
Produces a reference to the `null` constant, an `enumerable` containing no
elements.

#### **5.4.2 Call Expression**
```
function_name( rvalue_1, rvalue_2, ..., rvalue_n )
```
Calls the function or generator `function_name`, passing expressions `rvalue_1`,
`rvalue_2`, etc as arguments. If the method is a function, the expression
produces the value of the returned `enumerable`. If the method is a generator,
the expression produces the value of the `enumerable` that may be iterated to
retrieve the generated elements.

#### **5.4.3 Cache Expression**
```
^rvalue
```
The `^` caching operator enumerates the expression `rvalue` fully, and produces
an enumerable that contains a cached copy of each element retrieved from
`rvalue`. The main advantage of this is that the elements of `rvalue` do not
need to be evaluated again with each enumeration if the value is cached, which 
can lead to very significant performance gains. Note that `rvalue` itself will
still point to the original `enumerable`, not the cached version. To make use of
the cached `enumerable`, the result of the expression must be stored in a
variable.

## 6 Grammar

Below, the formal grammar for the Brainumerable language is layed out in
Backus-Naur form.

```XML
<top> ::= <global_dec> 
        | <top> <global_dec>

<global_dec> ::= <function_dec>
               | <var_dec>

<function_dec> ::= <sig> '{' <statements> '}'

<sig> ::= 'void' <identifier> '(' <identifier_list> ')'
        | 'function' <identifier> '(' <identifier_list> ')'
        | 'generator' <identifier> '(' <identifier_list> ')'

<identifier_list> ::= <identifier>
                    | <identifier_list> ',' <identifier>

<statements> ::= <statement>
               | <statements> <statement>

<statement> ::= '{ <statements> '}'
              | <var_dec> ';'
              | 'return' <expr> ';'
              | 'yield' <expr> ';'
              | '^' <expr> ';'
              | <call> ';'
              | 'for' '(' <identifier> ':' <expr> ')' <statement>

<call> ::= <identifier> '(' <arg_list> ')'

<arg_list> ::= <expr>
             | <arg_list> ',' <expr>

<var_dec> ::= <identifier> ';'
            | <identifier> '=' <expr> ';'

<expr> ::= 'null'
         | <call>
         | '^' <expr>
         | <identifier>
```

In the above, `<identifier>` matches any string composed of printable ASCII that
meets the following conditions:
1. The string does not include whitespace.
2. The string does not include the characters `{}(),^:;=`.
3. The string is not equal to any of the keywords `void`, `function`,
`generator`, `return`, `yield`, `for`, or `null`.

In addition, arbitrary whitespace may be included between any two rules.