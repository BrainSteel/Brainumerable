# The Brainumerable Project

Brainumerable is a programming language designed using only enumerables.

This repository will host the language specification, as well as an
implementation of the language written in C.

For information about the language itself, see the 
[Brainumerable Language Specification](./Language.md).

To see the original .NET implementation of a BF interpreter written using
only `IEnumerable<T>`s, which serves as inspiration for the language, see
the [DotNet](./DotNet) folder. Because the language can interpret and execute fully
standard BF with a tape of arbitrary length, the language is turing-complete.

## Build

Prerequisites: gcc

Run `make` in the top-level directory.