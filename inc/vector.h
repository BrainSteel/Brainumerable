
#ifndef VECTOR_H
#define VECTOR_H

#include "stdint.h"
#include "stddef.h"

#include "common.h"

#define V_DEFAULT_CAPACITY 16

#define V_GET( vector, index, type ) (type*)v_get( vector, index )

struct vector
{
    void* data;
    size_t elsize;
    uint32_t elcount;
    uint32_t capacity;
};

struct vector v_create_empty( size_t elsize );
struct vector v_create_default( size_t elsize );
struct vector v_create( size_t elsize, uint32_t capacity );

void v_destroy( struct vector* vect );

int v_resize( struct vector* vect, uint32_t capacity );
void* v_reserve( struct vector* vect, uint32_t count );
int v_push( struct vector* vect, const void* item );
void* v_get( const struct vector* vect, uint32_t index );

#endif
