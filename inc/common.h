#ifndef COMMON_H
#define COMMON_H

#include "stdint.h"

#define RETURN_SUCCESS 0
#define RETURN_FAIL -1

#define NEWLINE "\r\n"

#define SIZE_ARRAY( arr ) (( sizeof( arr ) / sizeof( *(arr) ) ))

enum log_level
{
    LOG_CRITICAL,
    LOG_NORMAL,
    LOG_VERBOSE,
    LOG_DIAGNOSTIC
};

enum log_type
{
    LOG_MESSAGE,
    LOG_WARNING,
    LOG_ERROR,
    LOG_ERROR_INTERNAL
};

struct file_location
{
    uint32_t line;
    uint32_t col;
};

void set_log_level( enum log_level level );
void compile_log( enum log_level level, enum log_type type, const char* fmt, ... );

#endif
