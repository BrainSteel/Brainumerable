
#ifndef LEX_H
#define LEX_H

#include "stdint.h"
#include "stddef.h"

#include "common.h"
#include "vector.h"

#define IDENTIFIER_INVALID 0xFFFFFFFFu

#define COMMENT_CHAR '#'

#define X_TOKEN_KEYWORDS                        \
    X( TOKEN_KEYWORD_VOID, "void" )             \
    X( TOKEN_KEYWORD_FUNCTION, "function" )     \
    X( TOKEN_KEYWORD_GENERATOR, "generator" )   \
    X( TOKEN_KEYWORD_RETURN, "return" )         \
    X( TOKEN_KEYWORD_YIELD, "yield" )           \
    X( TOKEN_KEYWORD_FOR, "for" )               \
    X( TOKEN_KEYWORD_NULL, "null" )

#define X_TOKEN_SYMBOLS                         \
    X( TOKEN_SYMBOL_BLOCK_OPEN, "{" )           \
    X( TOKEN_SYMBOL_BLOCK_CLOSE, "}" )          \
    X( TOKEN_SYMBOL_ARG_OPEN, "(" )             \
    X( TOKEN_SYMBOL_ARG_CLOSE, ")" )            \
    X( TOKEN_SYMBOL_SEPARATOR, "," )            \
    X( TOKEN_SYMBOL_ASSIGN, "=" )               \
    X( TOKEN_SYMBOL_CACHE, "^" )                \
    X( TOKEN_SYMBOL_ENUMERATE, ":" )            \
    X( TOKEN_SYMBOL_END_STATEMENT, ";" )    

#define X_TOKEN_RESERVED                        \
    X_TOKEN_KEYWORDS                            \
    X_TOKEN_SYMBOLS

#define X_TOKENS                                \
    X_TOKEN_RESERVED                            \
    X( TOKEN_IDENTIFIER, "" )

enum token_type
{
#define X( enum, str ) enum,
    X_TOKENS
#undef X
    NUM_TOKENS
};

struct token
{
    struct file_location location;
    enum token_type type;
    ptrdiff_t identifier;
};

struct token_set
{
    struct vector tokens;
    struct vector identifiers;
};

extern const char* token_type_names[NUM_TOKENS];

int lex_file( struct token_set* tokens, const char* filename );
void lex_destroy( struct token_set* tokens );

enum token_type token_get_type( struct token_set* set, uint32_t index );
char* token_get_identifier( struct token_set* set, uint32_t index );
    
#endif
