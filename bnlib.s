    .globl _start
    .data
msg:     .ascii "Hello, World\n"
len:     .long len - msg
    .text
_start:
    subq $48, %rsp              # Allocate 48 bytes on the stack
    
    movl $-11, %ecx
    call GetStdHandle           # GetStdHandle( -11 )
    
    movq %rax, %rcx
    leaq msg, %rdx
    movl len, %r8d
    leaq 36(%rsp), %r9
    movq $0, 32(%rsp)
    call WriteConsoleA          # WriteConsoleA( handle, msg, len, &dummy, 0 )
    
    movq $0, %rcx
    call ExitProcess            # ExitProcess( 0 )
    
