﻿using System.Collections.Generic;

namespace Brainumerable
{
    using IEnumerable = IEnumerable<object>;
    public static partial class Brainumerable
    {
        // This file contains builtins that are not possible
        // with only enumerables and enumerators. This includes
        // a CacheEnumeration() function whose only purpose is efficiency, and input/output.

        // If nothing ever uses CacheEnumeration(), the whole program slows to an 
        // unbearable crawl. Some people would consider that a reason not to
        // write a program with only enumerables.
        public static IEnumerable<T> CacheEnumeration<T>( this IEnumerable<T> enumerable )
        {
            return new List<T>( enumerable ).AsReadOnly( );
        }

        private static IEnumerable FromByte( byte size )
        {
            for ( int i = 0; i < size; i++ )
            {
                yield return null;
            }
        }

        private static byte ToByte( IEnumerable enumerable )
        {
            return (byte)System.Linq.Enumerable.Count( enumerable );
        }

        public static IEnumerable ReadChar( )
        {
            byte size = System.Convert.ToByte( System.Console.Read( ) );
            return FromByte( size ).CacheEnumeration( );
        }

        public static IEnumerable ReadByte( )
        {
            byte nextread = (byte)System.Console.Read( );
            byte total = 0;
            while ( nextread <= '9' && nextread >= '0' )
            {
                total *= 10;
                total += (byte)(nextread - System.Convert.ToByte( '0' ));
                nextread = (byte)System.Console.Read( );
            }

            return FromByte( total ).CacheEnumeration( );
        }

        public static void WriteByte( IEnumerable input )
        {
            System.Console.Write( ToByte( input ) );
        }

        public static void WriteChar( IEnumerable input )
        {
            System.Console.Write( System.Convert.ToChar( ToByte( input ) ) );
        }
    }
}
