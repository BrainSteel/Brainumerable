﻿using System.Collections.Generic;

namespace Brainumerable
{
    using IEnumerable = IEnumerable<object>;
    public static partial class Brainumerable
    {
        private static IEnumerable InstPointer { get; set; } = GetZero<object>( );

        private static IEnumerable<IEnumerable> Instructions { get; set; }

        private static IEnumerable DataPointer { get; set; } = GetZero<object>( );

        private static IEnumerable<IEnumerable> Data { get; set; } = GetContainer( GetZero<object>( ) );

        public static void Main( )
        {
            RunBF( ).CacheEnumeration( );
        }

        public static IEnumerable<IEnumerable> RunBF( )
        {
            Instructions = ReadProgram( ).CacheEnumeration( );

            InstPointer = InstPointer.CacheEnumeration( );

            CacheData( );

            foreach ( var instruction in NextInstruction( ) )
            {
                ExecuteInstruction( instruction );

                InstPointer = InstPointer.CacheEnumeration( );

                CacheData( );

                yield return instruction;
            }
        }

        // This function is not strictly necessary, but without it your BF
        // program probably takes a few years to terminate.
        public static void CacheData( )
        {
            Data = Data.CacheEnumeration( );
            DataPointer = DataPointer.CacheEnumeration( );
        }

        public static IEnumerable<IEnumerable> NextInstruction( )
        {
            foreach ( var loop in Infinity )
            {
                foreach ( var lt in GreaterOrEqualTo( InstPointer, Instructions ) )
                {
                    yield break;
                }

                yield return GetElement( Instructions, InstPointer ).CacheEnumeration( );
            }
        }

        public static IEnumerable<IEnumerable> ReadProgram( )
        {
            IEnumerable<IEnumerable> result = GetContainer( Zero );
            foreach ( var loop in Infinity )
            {
                IEnumerable input = ReadChar( );
                foreach ( var exc_check in EqualTo( input, Exclamation ) )
                {
                    yield break;
                }

                // We encode each instruction as a smaller number than its ASCII value.
                // This helps in efficiency of execution.
                foreach ( var item in EqualTo( input, PlusChar ) )
                {
                    yield return Plus;
                }

                foreach ( var item in EqualTo( input, MinusChar ) )
                {
                    yield return Minus;
                }

                foreach ( var item in EqualTo( input, LeftChar ) )
                {
                    yield return Left;
                }

                foreach ( var item in EqualTo( input, RightChar ) )
                {
                    yield return Right;
                }

                foreach ( var item in EqualTo( input, PeriodChar ) )
                {
                    yield return Period;
                }

                foreach ( var item in EqualTo( input, CommaChar ) )
                {
                    yield return Comma;
                }

                foreach ( var item in EqualTo( input, OpenChar ) )
                {
                    yield return Open;
                }

                foreach ( var item in EqualTo( input, CloseChar ) )
                {
                    yield return Close;
                }

                foreach ( var item in EqualTo( input, TildeChar ) )
                {
                    yield return Tilde;
                }
            }
        }

        public static void ExecuteInstruction( IEnumerable instruction )
        {
            foreach ( var item in EqualTo( instruction, Plus  ) )
            {
                var current = GetElement( Data, DataPointer ).CacheEnumeration( );
                current = IncrementByte( current ).CacheEnumeration( );
                Data = SetElement( Data, DataPointer, current );
                InstPointer = Increment( InstPointer );
                return;
            }

            foreach ( var item in EqualTo( instruction, Minus ) )
            {
                var current = GetElement( Data, DataPointer ).CacheEnumeration();
                current = DecrementByte( current ).CacheEnumeration();
                Data = SetElement( Data, DataPointer, current ).CacheEnumeration();
                InstPointer = Increment( InstPointer );
                return;
            }

            foreach ( var item in EqualTo( instruction, Left ) )
            {
                DataPointer = Decrement( DataPointer );
                InstPointer = Increment( InstPointer );
                return;
            }

            foreach ( var item in EqualTo( instruction, Right ) )
            {
                DataPointer = Increment( DataPointer );
                foreach ( var lesscheck in GreaterOrEqualTo( DataPointer, Data ) )
                {
                    Data = Add( Data, GetContainer( Zero ) );
                }
                InstPointer = Increment( InstPointer );
                return;
            }

            foreach ( var item in EqualTo( instruction, Period ) )
            {
                var current = GetElement( Data, DataPointer );
                WriteChar( current );
                InstPointer = Increment( InstPointer );
                return;
            }

            foreach ( var item in EqualTo( instruction, Comma ) )
            {
                var read = ReadChar( );
                Data = SetElement( Data, DataPointer, read );
                InstPointer = Increment( InstPointer );
                return;
            }

            foreach ( var item in EqualTo( instruction, Open ) )
            {
                var current = GetElement( Data, DataPointer );
                foreach ( var checkval in ToBool( current ) )
                {
                    InstPointer = Increment( InstPointer );
                    return;
                }

                var stack = Zero;

                foreach ( var loop in Infinity )
                {
                    InstPointer = Increment( InstPointer ).CacheEnumeration( );
                    var newinst = GetElement( Instructions, InstPointer ).CacheEnumeration( );
                    foreach ( var checkOpen in EqualTo( newinst, Open ) )
                    {
                        stack = Increment( stack ).CacheEnumeration( );
                    }

                    foreach ( var checkClose in EqualTo( newinst, Close ) )
                    {
                        foreach ( var checkStack in Not( stack ) )
                        {
                            InstPointer = Increment( InstPointer );
                            return;
                        }

                        stack = Decrement( stack ).CacheEnumeration( );
                    }
                }
            }

            foreach ( var item in EqualTo( instruction, Close ) )
            {
                var current = GetElement( Data, DataPointer );
                foreach ( var checkval in Not( current ) )
                {
                    InstPointer = Increment( InstPointer ).CacheEnumeration( );
                    return;
                }

                var stack = Zero;

                foreach ( var loop in Infinity )
                {
                    InstPointer = Decrement( InstPointer ).CacheEnumeration( );
                    var newinst = GetElement( Instructions, InstPointer ).CacheEnumeration( );
                    foreach ( var checkClose in EqualTo( newinst, Close ) )
                    {
                        stack = Increment( stack ).CacheEnumeration( );
                    }

                    foreach ( var checkOpen in EqualTo( newinst, Open ) )
                    {
                        foreach ( var checkStack in Not( stack ) )
                        {
                            InstPointer = Increment( InstPointer ).CacheEnumeration( );
                            return;
                        }

                        stack = Decrement( stack ).CacheEnumeration( );
                    }
                }
            }

            foreach ( var item in EqualTo( instruction, Tilde ) )
            {
                var count = Zero;
                foreach ( var loop in Infinity )
                {
                    WriteByte( GetElement( Data, count ) );
                    WriteChar( Space );

                    foreach ( var equalcheck in EqualTo( count, DataPointer ) )
                    {
                        WriteChar( NewLine );
                        InstPointer = Increment( InstPointer );
                        return;
                    }

                    count = Increment( count ).CacheEnumeration( );
                }
            }

            InstPointer = Increment( InstPointer );
        }

        #region Constants
        public static IEnumerable<T> GetZero<T>()
        {
            yield break;
        }

        public static IEnumerable<T> GetOne<T>()
        {
            yield return default( T );
        }

        public static IEnumerable<T> GetNum255<T>( )
        {
            foreach ( var item in Decrement( Num256 ) )
            {
                yield return default( T );
            }
        }

        public static IEnumerable Infinity
        {
            get
            {
                yield return null;
                foreach ( var item in Infinity )
                {
                    // Binary tree to avoid StackOverflow.
                    // Don't think about this too much.
                    yield return null;
                    yield return null;
                }
            }
        }

        public static readonly IEnumerable Zero = GetZero<object>( ).CacheEnumeration( );
        public static readonly IEnumerable One = GetOne<object>( ).CacheEnumeration( );
        public static readonly IEnumerable Num2 = Add( One, One ).CacheEnumeration( );
        public static readonly IEnumerable Num3 = Add( Num2, One ).CacheEnumeration( );
        public static readonly IEnumerable Num4 = Add( Num2, Num2 ).CacheEnumeration( );
        public static readonly IEnumerable Num5 = Add( Num4, One ).CacheEnumeration( );
        public static readonly IEnumerable Num6 = Add( Num4, Num2 ).CacheEnumeration( );
        public static readonly IEnumerable Num7 = Add( Num4, Num3 ).CacheEnumeration( );
        public static readonly IEnumerable Num8 = Add( Num4, Num4 ).CacheEnumeration( );
        public static readonly IEnumerable Num16 = Add( Num8, Num8 ).CacheEnumeration( );
        public static readonly IEnumerable Num32 = Add( Num16, Num16 ).CacheEnumeration( );
        public static readonly IEnumerable Num64 = Add( Num32, Num32 ).CacheEnumeration( );
        public static readonly IEnumerable Num128 = Add( Num64, Num64 ).CacheEnumeration( );
        public static readonly IEnumerable Num256 = Add( Num128, Num128 ).CacheEnumeration( );
        public static readonly IEnumerable Exclamation = Add( Num32, One ).CacheEnumeration( );
        public static readonly IEnumerable Plus = Zero;
        public static readonly IEnumerable Minus = One;
        public static readonly IEnumerable Left = Num2;
        public static readonly IEnumerable Right = Num3;
        public static readonly IEnumerable Period = Num4;
        public static readonly IEnumerable Comma = Num5;
        public static readonly IEnumerable Open = Num6;
        public static readonly IEnumerable Close = Num7;
        public static readonly IEnumerable Tilde = Num8;
        public static readonly IEnumerable PlusChar = Add( Add( Add( Num32, Num8 ), Num2 ), One ).CacheEnumeration( );
        public static readonly IEnumerable MinusChar = Add( Add( Add( Num32, Num8 ), Num4 ), One ).CacheEnumeration( );
        public static readonly IEnumerable LeftChar = Add( Add( Add( Num32, Num16 ), Num8 ), Num4 ).CacheEnumeration( );
        public static readonly IEnumerable RightChar = Add( Add( Add( Add( Num32, Num16 ), Num8 ), Num4 ), Num2 ).CacheEnumeration( );
        public static readonly IEnumerable PeriodChar = Add( Add( Add( Num32, Num8 ), Num4 ), Num2 ).CacheEnumeration( );
        public static readonly IEnumerable CommaChar = Add( Add( Num32, Num8 ), Num4 ).CacheEnumeration( );
        public static readonly IEnumerable OpenChar = Add( Add( Add( Add( Num64, Num16 ), Num8 ), Num2 ), One ).CacheEnumeration( );
        public static readonly IEnumerable CloseChar = Add( Add( Add( Add( Num64, Num16 ), Num8 ), Num4 ), One ).CacheEnumeration( );
        public static readonly IEnumerable TildeChar = Decrement( Decrement( Num128 ) ).CacheEnumeration( );
        public static readonly IEnumerable Space = Num32.CacheEnumeration( );
        public static readonly IEnumerable NewLine = Add( Num8, Num2 ).CacheEnumeration( );

        #endregion

        #region Math
        public static IEnumerable<T> Increment<T>( IEnumerable<T> enumerable )
        {
            foreach ( var olditem in enumerable )
            {
                yield return default( T );
            }

            yield return default( T );
        }

        public static IEnumerable<T> IncrementByte<T>( IEnumerable<T> enumerable )
        {
            var result = Increment( enumerable ).CacheEnumeration( );
            foreach ( var item in EqualTo( result, Num256 ) )
            {
                return GetZero<T>();
            }

            return result;
        }

        public static IEnumerable<T> Decrement<T>( this IEnumerable<T> enumerable )
        {
            var remove_first = Zero;
            foreach ( var item in enumerable )
            {
                foreach ( var toreturn in remove_first )
                {
                    yield return item;
                }

                remove_first = One;
            }
        }

        public static IEnumerable<T> DecrementByte<T>( IEnumerable<T> enumerable )
        {
            foreach ( var item in Not( enumerable ) )
            {
                return GetNum255<T>( ).CacheEnumeration();
            }

            return Decrement( enumerable );
        }

        public static IEnumerable<T> Add<T>( IEnumerable<T> a, IEnumerable<T> b )
        {
            foreach ( var item in a )
            {
                yield return item;
            }

            foreach ( var item in b )
            {
                yield return item;
            }
        }

        public static IEnumerable<T> SubtractStart<T, U>( IEnumerable<T> a, IEnumerable<U> b )
        {
            foreach ( var item in b )
            {
                a = Decrement( a );
            }

            return a;
        }
        #endregion

        #region Logic
        public static IEnumerable ToBool<T>( IEnumerable<T> val )
        {
            foreach ( var item in val )
            {
                return One;
            }

            return Zero;
        }

        public static IEnumerable Not<T>( IEnumerable<T> val )
        {
            foreach ( var item in val )
            {
                return Zero;
            }

            return One;
        }

        public static IEnumerable Or<T, U>( IEnumerable<T> a, IEnumerable<U> b )
        {
            foreach ( var item in a )
            {
                return One;
            }

            foreach ( var item in b )
            {
                return One;
            }

            return Zero;
        }

        public static IEnumerable And<T, U>( IEnumerable<T> a, IEnumerable<U> b )
        {
            foreach ( var aitem in a )
            {
                foreach ( var bitem in b )
                {
                    return One;
                }
            }

            return Zero;
        }
        #endregion

        #region Comparison
        public static IEnumerable LessThan<T, U>( IEnumerable<T> a, IEnumerable<U> b )
        {
            return ToBool( SubtractStart( b, a ));
        }

        public static IEnumerable LessOrEqualTo<T, U>( IEnumerable<T> a, IEnumerable<U> b )
        {
            return LessThan( a, Increment( b ).CacheEnumeration( ) );
        }

        public static IEnumerable GreaterThan<T, U>( IEnumerable<T> a, IEnumerable<U> b )
        {
            return LessThan( b, a );
        }

        public static IEnumerable GreaterOrEqualTo<T, U>( IEnumerable<T> a, IEnumerable<U> b )
        {
            return LessOrEqualTo( b, a );
        }

        public static IEnumerable EqualTo<T, U>( IEnumerable<T> a, IEnumerable<U> b )
        {
            return Not( Or( SubtractStart( a, b ), SubtractStart( b, a ) ) );
        }
        #endregion

        #region List Operations
        public static IEnumerable<T> GetContainer<T>( T item )
        {
            yield return item;
        }

        public static T GetFirst<T>( IEnumerable<T> enumerable )
        {
            foreach ( var item in enumerable )
            {
                return item;
            }

            return default( T );
        }

        public static IEnumerable<T> GetFirstN<T, U>( IEnumerable<T> enumerable, IEnumerable<U> N )
        {
            foreach ( var item in N )
            {
                yield return GetFirst( enumerable );

                enumerable = Decrement( enumerable );
            }
        }

        public static T GetElement<T, U>( IEnumerable<T> enumerable, IEnumerable<U> index )
        {
            return GetFirst( SubtractStart( enumerable, index ) );
        }

        public static IEnumerable<T> SetElement<T, U>( IEnumerable<T> enumerable, IEnumerable<U> index, T value )
        {
            foreach ( var item in GetFirstN( enumerable, index ) )
            {
                yield return item;
            }

            yield return value;

            foreach ( var item in SubtractStart( enumerable, Increment( index ) ) )
            {
                yield return item;
            }
        }
        #endregion
    }
}
