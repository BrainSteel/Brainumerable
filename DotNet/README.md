# Brainumerable

A fully standard BrainFuck interpreter written in C#, using only IEnumerables.

## The Rules

The following features of C# are used in the interpreter:
 - Types (and return types): `void`, `object`, `IEnumerable<T>`
 - Keywords and control: `foreach`, `yield return`, `yield break`, `return`, `null`,
   `default` (plus boring things like: `var`, `public`, `static`, `get`, `set`, `in`)
 - Functions, including recursive functions, generic functions, and extensions.
 - The built-in constructs defined in the Builtin file.

That's it! No LINQ, no more classes, no more types, no more built in functions, 
no more operators, no if, while, for, goto, switch, break, try, catch, finally, etc.
You know you're doing things right when you can't tell the difference between your
integers and your data structures.

This is truly C# in its purest form.

## Usage

The executable, `Brainumerable`, takes no command line arguments.
The BF program should be input to stdin, followed by an exclamation mark (`!`),
followed by the input of the BF program.
Examples:

Hello world (no input):

    >Brainumerable
    ++++++++[->>++++[>+>+>+>++>+++[<]>-]>>>+>+>+[<]<<]>>>>+>++++>.>---.+++++++..[->+>+<<]>+++.<<<.<<.>>>>++++[-<++++>]<-.>>.+++.>.--------.<<<<<.!
    
Output: `Hello, World!`
    
Add two ASCII characters ('1' + '2' = 'c'):

    >Brainumerable
    ,>,[-<+>]<.!12

Output: `c`

## Debugging

Yes, this interpreter even supports debugging! Kind of.
A `~` instruction prints the state of the tape to stdout, up to and including
the current data pointer.